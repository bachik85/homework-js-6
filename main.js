/* Метод forEach перебирает весь массив элементов что-бы сделать с ними какие то действия */

const arr = ['hello', 'world', 23, '23', null];
const newArr = arr.filter(n => typeof n !== 'string');

console.log(newArr);